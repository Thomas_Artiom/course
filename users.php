<?php 
include "db.php";

$errMsg = [];
$users = selectAll('users');


//Регастрация пользователя просто
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['reg-button'])){
    $name = trim($_POST['name']);
    $mail = trim($_POST['email']);
    $passF = trim($_POST['password-f']);
    $passS = trim($_POST['password-s']);

    if ($name === '' || $mail === '' || $passF === '' || $passS === ''){
        array_push($errMsg,"Заполните все поля!");
    } elseif (mb_strlen($passF, 'UTF8') <8 ){
        array_push($errMsg,"Пароль слишком короткий!(не менее 8 символов)");
    } elseif ($passF !== $passS){
        array_push($errMsg,"Введите одинаковые пароли!");
    } elseif (!preg_match('/[A-Z]+/', $passF)){
        array_push($errMsg,"Пароль не содержит большие буквы!");
    } elseif (!preg_match('/[0-9]+/', $passF)){
        array_push($errMsg,"Пароль не содержит цифры!");
    } elseif (!preg_match('/[!?]+/', $passF)){
        array_push($errMsg,"Пароль не содержит один из спец.символов(?,!)!");
    }
    else {
        $existence = selectOne('users', ['email' => $mail]);
        if (!empty($existence['email']) && $existence['email'] === $mail){
            array_push($errMsg,"Пользователь с данной почтой уже существует!");
        } 
        else{
            $pass = password_hash($passF, PASSWORD_DEFAULT);
            $user = [
                'name' => $name,
                'email' => $mail,
                'password' => $pass
                ];
            $id = insert('users', $user);
            header('location: http://localhost/coursev2/succ.php');
        }
    }
} else {
    $name = '';
    $email = '';
}