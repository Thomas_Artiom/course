<?php
    include "users.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <title>Coursev2</title>
</head>
<body>
<main>
<section class="registration-section">
        <div class="registration-wrapper">
        <h2 class="registration-heading">Регистрация</h2>
        <form action="reg.php" class="registration-form" method="post">
            <div class="registration-labels">
                <p><?php include 'errorInfo.php'; ?></p>
            </div>
            <div class="registration-labels">
                <label for="formGroupExampleInput" class="registration-label">Имя</label>
                <input name="name" type="text" class="registration-input" id="formGroupExampleInput" placeholder="Введите ваше имя..." required>
            </div>
            <div class="registration-labels">
                <label for="exampleInputEmail1" class="registration-label">Логин(Ваша почта)</label>
                <label for="" class="registration-text">Будет использоваться в качестве логина</label>
                <input name="email" type="email" class="registration-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите вашу почту..." required>
            </div>
            <div class="registration-labels">
                <label for="exampleInputPassword1" class="registration-label">Введите пароль</label>
                <label for="" class="registration-text">Большие латинские буквы, цифры и знаки ?! обязательны</label>
                <input name="password-f" type="password" class="registration-input" id="exampleInputPassword1" placeholder="Введите ваш пароль..." required>
            </div>
            <div class="registration-labels">
                <label for="exampleInputPassword2" class="registration-label">Повторите введенный пароль</label>
                <input name="password-s" type="password" class="registration-input" id="exampleInputPassword2" placeholder="Повторите ваш пароль..." required>
            </div>
            <div class="registration-buttons">
                <button type="submit" class="registration-button" name="reg-button">Зарегистрироваться</button>
            </div>
        </form>
        </div>
    </section>
</main>
</body>
</html>
