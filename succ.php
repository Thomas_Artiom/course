<?php
    include "users.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <title>Coursev2</title>
</head>
<body>
<main>
<section class="registration-section">
        <div class="registration-wrapper">
        <h2 class="registration-heading">Аккаунт успешно зарегистрирован!</h2>
        <h2 class="registration-head">Список пользователей:</h2>
        <ul>
            <?php foreach ($users as $key => $user): ?>
                <li class="registration-users">ИД:<?=$user['id'];?>, ИМЯ:<?=$user['name'];?>, ПОЧТА:<?=$user['email'];?>, Пароль захеширован!</li>
            <?php endforeach; ?>
        </ul>
        </div>
    </section>
</main>
</body>
</html>
